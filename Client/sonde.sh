#!/bin/bash

# Ecrit par Tony "Georgie" Reynier, dernière mise à jour: 19 mars 2020
# Usage pédagogique dans le cadre d'un projet scolaire en Licence 2 Informatique
# à l'Université d'avignon

# sonde permettant l'extraction des informations suivantes:
# - Les informations du CPU
# - Les informations du Disque Dur
# - Les informations de la RAM
# - Le nombre de process en cours d'execution (via bash)
# - Le nombre d'utilisateurs connectés (via bash)

# Récuperation des adresses IP et MAC pour distinguer les machines
# (Réseau local)
servAdr="192.168.1.61"
Interface="enp0s3"
MACadr=$(cat /sys/class/net/$Interface/address)
IPadr=$(hostname -I)
HostID=$(hostname -I | cut -f 4 -d ".")

# Collecte de données toutes les 60 secondes et les stockent sous format json
# Et envoi de requête HTTP contenant ces données vers le serveur
chmod 777 RAMtotal.py RAMused.py HDDfs.py HDDpart.py HDDtotal.py HDDused.py CPUpercent.py CPUfreq.py


echo "Collecte de données toutes les minutes."
echo "Utilisez [CTRL+C] pour y mettre fin..."
while true
do
	echo -e "{" > data.json
	
	echo -e "  \"Adress\": {" >> data.json
	echo -e "    \"IP\": \"${IPadr}\"," >> data.json
	echo -e "    \"HostID\": \"${HostID}\"," >> data.json
	echo -e "    \"MAC\": \"${MACadr}\"" >> data.json
	echo -e "  }," >> data.json

	echo -e "  \"TimeStamp\": {" >> data.json
	currentDate=$(date +"%Y-%m-%d")
	echo -e "    \"date\": \"${currentDate}\"," >> data.json
	currentTime=$(date +"%H:%M:%S")
	echo -e "    \"time\": \"${currentTime}\"" >> data.json
	echo -e "  }," >> data.json
	
	echo -e "  \"CPU\": {" >> data.json
	CPUpercent=$(python CPUpercent.py)
	echo -e "    \"usagePercent\": \"${CPUpercent}\"," >> data.json
	CPUfreq=$(python CPUfreq.py)
	echo -e "    \"frequency\": \"${CPUfreq}\"" >> data.json
	echo -e "  }," >> data.json
	
	declare -a filesysArray
	filesysArray=($(python HDDfs.py))
	declare -a partitionArray
	partitionArray=($(python HDDpart.py))
	declare -a HDDtotalArray
	HDDtotalArray=($(python HDDtotal.py))
	declare -a HDDusedArray
	HDDusedArray=($(python HDDused.py))
	echo -e "  \"HDD\": {" >> data.json
	arraySize=$((${#partitionArray[@]}-1))
	for ((i=0; i<=arraySize; i++))
	do
		echo -e "    \"partition[${i}]\": {" >> data.json
		echo -e "      \"fileSystem\": \"${filesysArray[i]}\", " >> data.json
		echo -e "      \"mountPoint\": \"${partitionArray[i]}\", " >> data.json
		echo -e "      \"totalSpace\": \"${HDDtotalArray[i]}\", " >> data.json
		echo -e "      \"usedSpace\": \"${HDDusedArray[i]}\"" >> data.json
		if [ $arraySize -eq $i ]
		then
			echo -e "    }" >> data.json
		else
			echo -e "    }," >> data.json
		fi
	done
	echo -e "  }," >> data.json
	
	echo -e "  \"RAM\": {" >> data.json
	RAMtotal=$(python RAMtotal.py)
	echo -e "  \"total\": \"${RAMtotal}\"," >> data.json
	RAMused=$(python RAMused.py)
	echo -e "  \"used\": \"${RAMused}\"" >> data.json
	echo -e "  }," >> data.json

	nbProcess=$(ps a | wc -l)
	echo -e "  \"nbOngoingProcess\": \"${nbProcess}\"," >> data.json
	
	nbUsers=$(users | wc -l)
	echo -e "  \"nbConnectedUsers\": \"${nbUsers}\"" >> data.json

	echo -e "}" >> data.json
	
	curl -X POST http://${servAdr}/index.php -d '@data.json' > /dev/null
	
	sleep 60;
done
