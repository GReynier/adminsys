#!/bin/bash

servAdr="192.168.1.61" 

curl -o jsonData.json http://${servAdr}/index.php


# Création de la base de données des sondes
# et recuperation des données

echo "
CREATE DATABASE IF NOT EXISTS probeData;
CREATE TABLE IF NOT EXISTS HDDPartitionsTable
(
	ID INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
	mountPoint VARCHAR(100),
	totalSpace INTEGER,
	usedSpace INTEGER,
	
);
CREATE TABLE IF NOT EXISTS OtherDataTable
(

);
.quit" | sqlite db.sql



# Création de la base de données des alertes CERT
# et recuperation des données

echo "
CREATE DATABASE IF NOT EXISTS certData;
.quit " | sqlite cert.sql
